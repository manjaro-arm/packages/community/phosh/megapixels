# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Martijn Braam <martijn@brixit.nl>
# Contributor: Oliver Smith <ollieparanoid@postmarketos.org>

pkgname=megapixels
_app_id=org.postmarketos.Megapixels
pkgver=1.8.3
pkgrel=1
pkgdesc="A GTK4 camera application that knows how to deal with the media request API."
arch=('x86_64' 'armv7h' 'aarch64')
url="https://gitlab.com/megapixels-org/Megapixels"
license=('GPL-3.0-only')
depends=('gtk4' 'feedbackd' 'hicolor-icon-theme' 'libepoxy' 'libraw' 'libtiff' 'wayland' 'zbar')
makedepends=('git' 'meson')
checkdepends=('appstream')
optdepends=('imagemagick' 'perl-image-exiftool')
replaces=('pinhole' 'gnome-camera' 'megapixels-ppp')
source=("git+https://gitlab.com/megapixels-org/Megapixels.git#tag=$pkgver"
        'https://github.com/kgmt0/megapixels/commit/06230f3a02cffdf8b683f85cb32fc256d73615d9.patch'
        'https://github.com/kgmt0/megapixels/commit/27a1e606d680295e0b4caceadf74ff5857ac16b2.patch'
        'd8b35bc223989cb165ba1b0716ab9f0ca9c43e53.patch'
        "90-${pkgname}.rules")
sha256sums=('91623dff3693981e7e2da8319f41a2d3657d85c1ce1d344ed35b2e17f65035a8'
            '9fb2270f439066b42431cda10b4d62c3baae92609d74c00e41670e1d9859140b'
            '89def2d0f14b0ecf92c408998900ebd776529adb2f94d09bc15f9a1d6aec9490'
            '964c945e9274edc4909318453acd58eb38b325b95d499f856c551c173b9359eb'
            'c9ff69c5963f6231ac6a7de5adc3e0327f47b501de9b084b49546bfb00e7fc58')

prepare() {
  cd Megapixels

  # patches here
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    msg2 "Applying patch: $src..."
    patch -Np1 < "../$src" # || true
  done

  pwd

  # 6.1 changes selfie cam name
  # https://github.com/megous/linux/commit/59ee4accb3997098c7b65fbf529ef3033ab1fd5a
  sed -i -e 's/m00_f_ov8858/ov8858/g' 'config/pine64,pinephone-pro.ini'
}

build() {
  arch-meson Megapixels build
  meson compile -C build
}

check() {
  appstreamcli validate --no-net "Megapixels/data/${_app_id}.metainfo.xml" || :
  desktop-file-validate "Megapixels/data/${_app_id}.desktop"
}

package() {
  meson install -C build --destdir "${pkgdir}"

  install -Dm644 "${srcdir}/90-${pkgname}.rules" -t "${pkgdir}/usr/lib/udev/rules.d/"
}
